from ..helpers import oAuthDatabase, getSentinelLogger
from ..const import __version__, __author__, __description__

class BaseOAuth(object):
    def __init__(self):
        
        self.USR_AGNT = __description__ + " v" + __version__ + " by " + __author__

        # Initialize the logger
        self.logger = getSentinelLogger()
        self.logger.debug('Initialized the BaseOAuth')

    def login(self):
        try:
            import praw, prawcore
            self.logger.debug('Attemping to login with: {}'.format(self.UNAME))
            r = praw.Reddit(user_agent=self.USR_AGNT,
                            client_id=self.APP_ID,
                            client_secret=self.APP_SECRET,
                            username=self.UNAME,
                            password=self.PASSWD)

            self.logger.debug('{} | Created the Login'.format(r.user.me()))
            return r
        except prawcore.exceptions.OAuthException:
            self.logger.critical('Unable to login to account. User: {} | Pass: {} | client_id: {} | client_secret: {}'.format(self.UNAME, self.PASSWD, self.APP_ID, self.APP_SECRET))
            import sys
            sys.exit()

    def set_info(self, id, secret, username, password):
        self.APP_ID, self.APP_SECRET, self.UNAME, self.PASSWD = id, secret, username, password
        self.logger.debug('Set the login info')

class oAuth(object):
    def __init__(self, id):
        self.db = oAuthDatabase()
        self.usernames = self.db.get_accounts(id)
        self.logger = getSentinelLogger()

        self.accounts = []
        for i in self.usernames:
            x = BaseOAuth()
            x.set_info(*i)
            self.accounts.append(x)
        
        self.logger.debug('Retrieved the logins from the database')