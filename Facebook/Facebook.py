from ..objects import MediaProcess, FacebookAPIProcess
from ..helpers import getSentinelLogger


class Facebook(MediaProcess):
    def __init__(self):
        # Initialize the logger
        self.logger = getSentinelLogger()

        mediaURLs = ['facebook.com']
        self.logger.debug('Initializing Facebook API Datapull')
        super(Facebook, self).__init__(FacebookAPIProcess, mediaURLs)
