from ..objects import MediaProcess, EtsyAPIProcess
from ..helpers import getSentinelLogger


class Etsy(MediaProcess):
    def __init__(self):
        # Initialize the logger
        self.logger = getSentinelLogger()

        mediaURLs = ['etsy.com']
        self.logger.debug('Initializing Etsy API Datapull')
        super(Etsy, self).__init__(EtsyAPIProcess, mediaURLs)
