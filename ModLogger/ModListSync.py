from ..helpers import getSentinelLogger, application


class ModListSync(object):
    def __init__(self, r):  # subs is a list of strings
        self.r = r
        self.me = self.r.user.me()

        self.logger = getSentinelLogger()
        self.db = application()

    def gather_items(self):
        for subreddit in self.r.user.moderator_subreddits():
            perms = list(self.r.subreddit(subreddit).moderator())
            for mod in perms:
                thing_dict = {
                    "mod_username": str(mod),
                    "subreddit": str(subreddit).lower(),
                }
                try:
                    self.db.insert_about_moderators(thing_dict)
                    self.logger.info(f"{self} Added Mod: {mod}, Sub: {subreddit} to about_moderators table")
                except Exception as err:
                    self.logger.exception(f"Error logging {mod} to database. Error: {err}")

    def __str__(self):
        return "ModListSync ({})".format(self.me)

    def log(self):
        self.logger.info("{} | Force ModListSync started".format(self.me))
        self.gather_items()
