import requests, json
from retrying import retry

from .SentinelLogger import getSentinelLogger
from .database import SlackHooks

class SlackNotifier(object):
    def __init__(self):
        self.database = SlackHooks()
        
    @property
    def hook_table(self):
        hooks = self.database.getHooks()
        #make this a property that gets the new hooks on call
        ht = {}

        for subreddit, url, channel in hooks:
            ht[subreddit] = self._send_message(url, channel)

        return ht

    def _send_message(self, url, channel):

        # @retry(stop_max_attempt_number=3)
        def send_message_closure(message):
            headers = {'content-type': 'application/json'}

            if 'discordapp' in url:
                posturl = url + '/slack'
                payload = {"username": "TheSentinelBot", "text": u"*Attention:* _The following item has been removed._ \n*Channel Author:* {media_author} \n*Reddit Author:* `{author}` \n*Subreddit:* {subreddit} \n*Reddit Link:* {permalink} \n*Report Data:* https://layer7.solutions/blacklist/reports/#subject={media_channel_id}".format(**message)}
                response = requests.post(posturl, data=json.dumps(payload), headers=headers).text
            else:
                posturl = url
                payload = {"channel": channel, "username": "TheSentinelBot", "text": u"*Attention:* _The following item has been removed._ \n*Channel Author:* {media_author} \n*Reddit Author:* `{author}` \n*Subreddit:* {subreddit} \n*Reddit Link:* {permalink} \n*Report Data:* https://layer7.solutions/blacklist/reports/#subject={media_channel_id}".format(**message)}
                response = requests.post(posturl, data=json.dumps(payload), headers=headers)

        return send_message_closure

    def send_message(self, subreddit, message):
        try:
            self.hook_table[subreddit](message)
        except requests.exceptions.HTTPError:
            pass
        except KeyError:
            pass