from ..helpers import getSentinelLogger, ModmailArchiverDB
from datetime import datetime
from dateutil import parser
import time
import psycopg2
import prawcore


class ModmailArchiver(object):
    def __init__(self, r, subs):  # subs is a list of strings
        self.r = r
        self.me = str(self.r.user.me())
        self.logger = getSentinelLogger()
        self.db = ModmailArchiverDB()

    def __str__(self):
        return "Modmail ({})".format(self.me)

    def legacyModMail(self, limit):
        try:
            legacy_modmail_generator = self.r.subreddit('mod').mod.inbox(limit=limit)
        except AttributeError:
            return

        self.logger.debug('{} | Getting Legacy Modmail History.'.format(self))
        arg_dicts = []

        for mail in legacy_modmail_generator:
            if not self.db.is_logged(mail.fullname):
                arg_dict = {
                    "thing_id": mail.fullname,
                    "message_root_thing_id": mail.first_message_name,
                    "message_from": str(mail.author),
                    "message_to": str(mail.dest).replace("#", "r/") if str(
                        mail.dest).startswith("#") else str(mail.dest),
                    "created_utc": datetime.utcfromtimestamp(mail.created_utc),
                    "subject": mail.subject,
                    "body": mail.body,
                    "parent_thing_id": mail.parent_id,
                    "subreddit": str(mail.subreddit)
                }
                if mail.fullname not in arg_dicts:
                    arg_dicts.append(arg_dict)
                    self.db.log_items([arg_dict])
                    self.logger.debug(
                        '{me} | Added Modmail ID to process: {thing_id}'.format(
                            me=self.me, thing_id=mail.fullname))
            for reply in mail.replies:
                if not self.db.is_logged(reply.fullname):
                    reply_dict = {
                        "thing_id": reply.fullname,
                        "message_root_thing_id": reply.first_message_name,
                        "message_from": str(reply.author),
                        "message_to": str(reply.dest).replace("#", "r/") if str(
                            reply.dest).startswith("#") else str(reply.dest),
                        "created_utc": datetime.utcfromtimestamp(reply.created_utc),
                        "subject": reply.subject,
                        "body": reply.body,
                        "parent_thing_id": reply.parent_id,
                        "subreddit": str(reply.subreddit)
                    }
                    if reply.fullname not in arg_dicts:
                        arg_dicts.append(reply_dict)
                        self.db.log_items([reply_dict])
                        self.logger.debug(
                            '{me} | Added Modmail ID to process: {thing_id}'.format(
                                me=self.me, thing_id=reply.fullname))

        # Tally up
        self.total_logged += len(arg_dicts)

    def newModMail(self, limit):
        # New modmail
        # `self.r.subreddit('layer7')` has to be some subreddit the bots mod, doesn't matter if that sub uses new modmail or not.
        # a sub or 'all' gets all modmail, plus the subs listed in `other_subreddits`.
        # a sub of 'mod' is not accepted, nor does it accept a sub that user does not have permissions to.
        # also doesn't accept a modMailMulti or listing multiple subs together, ie "sub1+sub2+sub3" as it ignores those and only uses whats
        # listed in 'other_subreddits'

        for mail in self.r.subreddit('layer7').modmail.bulk_read():

            try:
                self.logger.debug('{} | Getting New Modmail History.'.format(self))
                arg_dicts = []
                participant = str(mail.participant)
                for reply in mail.messages:
                    if not self.db.is_logged(reply.fullname):
                        reply_dict = {
                            "thing_id": reply.fullname,
                            "message_root_thing_id": mail.fullname,
                            "message_from": str(reply.author),
                            "message_to": participant,
                            "created_utc": parser.parse(reply.date),
                            "subject": mail.subject,
                            "body": reply.body_markdown,
                            "parent_thing_id": mail.fullname,
                            "subreddit": str(mail.owner)
                        }
                        if reply.fullname not in arg_dicts:
                            arg_dicts.append(reply_dict)
                            self.db.log_items([reply_dict])
                            self.logger.debug(
                                '{me} | Sub: {sub} | Added Modmail ID to process: {thing_id}'.format(
                                    me=self.me, sub=str(mail.owner),
                                    thing_id=reply.fullname))

                # Tally up
                self.total_logged += len(arg_dicts)

            except prawcore.exceptions.Forbidden:
                self.logger.debug(
                    '{} | Error getting mail. Either missing mail permission or no messages.'.format(
                        self))
                continue

    def gather_items(self, limit):
        self.total_logged = 0
        try:
            # Legacy modmail
            self.legacyModMail(limit)

            # New Modmail Support
            self.newModMail(limit)

        except prawcore.exceptions.Forbidden:
            self.logger.error(
                '{} | Mail empty or missing `mail` permission'.format(self))

        except psycopg2.InternalError:
            self.logger.error('{} | PSQL Error. Sleeping 60'.format(self))
            time.sleep(60)

        except:
            self.logger.error(
                '{} | Global error retrieving modmail history'.format(self))

    def log(self, limit=300, author=None):
        if (not limit):
            self.logger.info("Force Mod Mail History started".format())
        self.gather_items(limit)
        if self.total_logged > 3:
            self.logger.info("{} | Added {} items to modmail archive".format(self,
                                                                             self.total_logged))
        if (not limit) and author and self.total_logged > 0:
            self.logger.info(
                "{} | Force Mod Mail History complete, {} inserted".format(self,
                                                                           self.total_logged))
            self.r.redditor('Layer7Solutions').message('Force Modmail History Results',
                                                       'Finished, {} inserted'.format(
                                                           self.total_logged))
        elif self.total_logged:
            self.logger.debug('{me} | Processed {amount} Modmail things'.format(me=self,
                                                                                amount=self.total_logged))

