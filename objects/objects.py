import configparser
import os, re, json
import requests
import memcache # https://pypi.python.org/pypi/python-memcached
from memcached_stats import MemcachedStats # https://github.com/dlrust/python-memcached-stats
import tweepy
import facebook
import praw
import random

from ..objects import datapulls

from ..helpers import Zion, getSentinelLogger, TheTraveler, Utility
from ..exceptions import InvalidAddition
from ..RabbitMQ import Rabbit_Producer

# Memcache Server Host
serverhost = '127.0.0.1'


class MediaProcess(object):
    def __init__(self, APIProcess, mediaURLs):
        # Initialize the logger
        self.logger = getSentinelLogger()

        self.db = SentinelDatabase()
        self.APIProcess = APIProcess()
        self.mediaURLs = mediaURLs

        self.logger.debug('Initialized MediaProcess')

    def validateURL(self, url):
        self.logger.debug(u'Checking if URL in MediaURLs list. URL: {}'.format(url))
        for i in self.mediaURLs:
            if i.lower() in url.lower():
                self.logger.debug("URL valid for {}".format(url))
                return True
        return False

    def hasBlacklisted(self, url, thing, RabbitUName, RabbitPWord, SentinelInstances):
        # takes string, dict
        if not self.validateURL(url):
            #self.logger.debug('Not valid media URL')
            return False
        try:
            channels = self.APIProcess.getInformation(url)
        except requests.exceptions.HTTPError:
            self.logger.warning("No information found for url - {}".format(url))
            return False
        for i in channels:
            blacklisted = self.db.isBlacklisted(str(thing.subreddit), **i)
            if blacklisted:
                self.logger.debug('Channel is Blacklisted. URL: {}'.format(url))
                return True
            else:
                # Send to Dirtbag
                try:
                  
                    thingDict = self.getThingData(thing, SentinelInstances)

                    temp = {
                        'ThingID': thingDict['ThingID'],
                        'Subreddit': thingDict['Subreddit'],
                        'EntryTime': thingDict['Created_UTC'],
                        'PermaLink': thingDict['PermaLink'],
                        'Author':{
                            'Name': thingDict['Author']['Name'],
                            'Created': thingDict['Author']['Created'],
                            'CommentKarma': thingDict['Author']['CommentKarma'],
                            'LinkKarma': thingDict['Author']['LinkKarma']
                        },
                        'MediaID': i['media_id'],
                        'MediaChannelID' : i['media_channel_id'],
                        'MediaChannelName' : i['media_author'],
                        'MediaPlatform' : i['media_platform']
                        }

                    try:
                        # Initializes the Dirtbag Rabbit Producer
                        dirtbagProducer = Rabbit_Producer(RabbitUName, RabbitPWord, exchange='Sentinel', routing_key='Dirtbag_ToAnalyze', QueueName='Dirtbag')
                        dirtbagProducer.send(json.dumps(temp))
                        self.logger.debug(u'Sent to Dirtbag for Analysis: {}'.format(temp))
                        dirtbagProducer.connection.close()
                    except Exception as e:
                        self.logger.error('Unable to create/send data to Dirtbag')
                        dirtbagProducer = Rabbit_Producer(RabbitUName, RabbitPWord, exchange='Sentinel', routing_key='Dirtbag_ToAnalyze', QueueName='Dirtbag')
                        dirtbagProducer.send(json.dumps(temp))
                        self.logger.debug(u'Sent to Dirtbag for Analysis: {}'.format(temp))
                        dirtbagProducer.connection.close()

                except KeyError:
                    self.logger.error('Key error with data')
                except Exception:
                    self.logger.error('Unable to send data to Dirtbag')
        # Default return false
        return False

    def getThingData(self, thing, SentinelInstances):
        try:
            thingDict = {}

            TSBPair = random.choice(SentinelInstances)
            instance = TSBPair[0]

            try:
                author = instance.r.redditor(str(thing.author))
                CommentKarma = author.comment_karma
                LinkKarma    = author.link_karma
                AuthDate     = int(author.created_utc)
            except Exception as e:
                self.logger.warning('{} | Unable to retrieve user account. {} may be shadowbanned/suspended/deleted'.format(instance, str(thing.author)))
                CommentKarma = None
                LinkKarma    = None
                AuthDate     = None
            
            if isinstance(thing, praw.models.Submission):
                perma = thing.shortlink
            else:
                perma = thing.permalink

            thingDict = {'ThingID': thing.fullname,
                         'Subreddit': str(thing.subreddit),
                         'Created_UTC': int(thing.created_utc),
                         'PermaLink': perma,
                         'Author':{
                            'Name': str(thing.author),
                            'Created': AuthDate,
                            'CommentKarma': CommentKarma,
                            'LinkKarma': LinkKarma}
                         }

            self.logger.debug('{} | Successfully created thingDict for author {}'.format(instance, author))

        except Exception:
            self.logger.error('Error creating thingDict while getting author data for {}'.format(str(thing.author))) 
        finally:
            return thingDict   

    def addToBlacklist(self, subreddit, url, playlist=False):
        if not self.validateURL(url):
            return False
        channels = self.APIProcess.getInformation(url)
        if len(channels) != 1 and not playlist:
            self.logger.warning(u'Invalid blacklist addition; either playlist or owner could not be found. URL: {}'.format(url))
            raise InvalidAddition("Invalid blacklist addition; either playlist or owner could not be found - %s" % url)
        for ch in channels:
            self.logger.debug('Attemping to add to DB')
            self.db.addBlacklist(subreddit, **ch)

    def getInformation(self, url):
        self.logger.debug(u'Attemping to getInformation(). URL: {}'.format(url))
        if not self.validateURL(url):
            return []
        else:
            return self.APIProcess.getInformation(url)


class APIProcess(object):
    def __init__(self, API_URLS, regexs, data_pulls):
        # Initialize the logger
        self.logger = getSentinelLogger()

        self.API_URLS = API_URLS
        self.regexs = regexs
        self.data_pulls = data_pulls

        self.api_key = None
        self.headers = None

    def _getJSONResponse(self, data, key):
        if isinstance(self.api_key, list):
            apikey = random.choice(self.api_key)
        else:
            apikey = self.api_key
        self.logger.info(f"APIProcess: API Key used: {apikey[0:5]}...{apikey[-10:]} | URL: {self.API_URLS[key][0:40]}")
        response = requests.get(self.API_URLS[key].format(data, apikey), headers=self.headers)

        if response.status_code != 200:
            if response.status_code == 403:
                # This is likely a response from Google due to going over the quota,
                # so instead of flooding the Sentry/Exception logs, just ignore it
                self.logger.info(f"API Error {response.status_code} - Ignoring (likely over Google quota)")
                response.raise_for_status()
            else:
                self.logger.exception(f"Get API Data Error. Error Code: {response.status_code}")
                response.raise_for_status()

        return key, response.json()

    #(key, data)
    def _getData(self, url):
        self.logger.debug('Getting URL Redirect Data')
        try:
            url = requests.get(url).url #deals with redirects
        except requests.exceptions.ConnectionError:
            raise KeyError("Problem resolving url. No match.")
        try:
            for i in self.regexs:
                match = re.search(self.regexs[i], url)
                if match:
                    self.logger.debug('Match Found')
                    break
            return (i, match.group(1))
        except AttributeError:
            self.logger.debug(u'No Match Found. URL: {}'.format(url))
            raise KeyError("No match found - %s" % url)

    def getInformation(self, url):
        #self.logger.debug(u'Getting Information. URL: {}'.format(url))
        try:
            key, data = self._getData(url)
        except KeyError:
            return []
        key, jsonResponse = self._getJSONResponse(data, key)
        try:
            alldata = self.data_pulls[key](jsonResponse) or []
        except TypeError:
            return []
        except KeyError:
            return []
        if key == 'playlist':
            self.logger.debug('Getting Playlist Data')
            key, jsonResponse = self._getJSONResponse(data, 'playlist videos')
            try:
                alldata += self.data_pulls['playlist videos'](jsonResponse)
            except TypeError:
                pass
        if alldata:
            self.logger.debug("Found some data!")
        return alldata


class TwitterAPIProcess(APIProcess):
    def __init__(self):

        self.logger = getSentinelLogger()


        Config = configparser.ConfigParser()
        mydir = os.path.dirname(os.path.abspath(__file__))
        Config.read(os.path.join(mydir, '..', "global_config.ini"))
        consumer_key = Config.get('TwitterAPI', 'CONSUMER_KEY')
        consumer_secret = Config.get('TwitterAPI', 'CONSUMER_SECRET')
        access_token = Config.get('TwitterAPI', 'ACCESS_TOKEN')
        access_token_secret = Config.get("TwitterAPI", "ACCESS_TOKEN_SECRET")

        auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_token_secret)
        self.api = tweepy.API(auth)

        Twitter_URLS = {
            'user': lambda x: self.api.get_user(x),
        }

        regexs = {
            'user': r"r\.com\/(.+?)(?:\/|\?|\&|$|#)"
        }

        super(TwitterAPIProcess, self).__init__(Twitter_URLS, regexs, datapulls.TwitterAPIPulls)

    def getInformation(self, url):
        #self.logger.debug(u'Getting Twitter Information. URL: {}'.format(url))
        try:
            key, data = self._getData(url)
        except KeyError as e:
            return []
        try:
            obj = self.API_URLS[key](data)
        except tweepy.error.TweepError:
            return []
        try:
            alldata = self.data_pulls[key](obj) or []
        except TypeError:
            return []

        
        return alldata


class VidmeAPIProcess(APIProcess):
    def __init__(self):

        self.logger = getSentinelLogger()

        Vidme_URLS = {
            'video': 'https://api.vid.me/videoByUrl?url={}',
        }

        regexs = {
            'video': r'(https:\/\/vid\.me\/.*)',
        }

        super(VidmeAPIProcess, self).__init__(Vidme_URLS, regexs, datapulls.VidmeAPIPulls)


class TwitchAPIProcess(APIProcess):
    def __init__(self):

        self.logger = getSentinelLogger()

        Twitch_URLS = {
            'user': 'https://api.twitch.tv/kraken/users?login={}',
        }
        
        regexs = {
            'user': r'\.tv\/(.+?)(?:$|\/)',
        }

        Config = configparser.ConfigParser()
        mydir = os.path.dirname(os.path.abspath(__file__))
        Config.read(os.path.join(mydir, '..', "global_config.ini"))
        api_key = Config.get('TwitchAPI', 'AUTH_KEY')
        

        super(TwitchAPIProcess, self).__init__(Twitch_URLS, regexs, datapulls.TwitchAPIPulls)
        self.headers = {'Accept': 'application/vnd.twitchtv.v5+json', 'Client-ID': api_key}


class GAPIProcess(APIProcess):
    def __init__(self):
        # Initialize the logger
        self.logger = getSentinelLogger()

        GAPI_URLS = {
            'channel': 'https://www.googleapis.com/youtube/v3/channels?part=snippet&id={}&fields=items(id%2Csnippet%2Ftitle)&key={}', #GOOD,
            'video': 'https://www.googleapis.com/youtube/v3/videos?part=snippet&id={}&fields=items(snippet(channelId%2CchannelTitle)%2Cid)&key={}', #GOOD
            'playlist': 'https://www.googleapis.com/youtube/v3/playlists?part=snippet&id={}&fields=items(snippet(channelId%2CchannelTitle)%2Cid)&key={}', # GOOD
            'playlist videos': 'https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId={}&fields=items(contentDetails%2FvideoId%2Csnippet(channelId%2CchannelTitle)%2Cid)&key={}', #GOOD
            'username': 'https://www.googleapis.com/youtube/v3/channels?part=snippet&forUsername={}&fields=items(id%2Csnippet%2Ftitle)&key={}' #GOOD}
        }
        regexs = {
            'channel': r'''(?i)channel\/(.*?)(?:\/|\?|$)''',
            'playlist': r'(?<!watch).*?list=((?!videoseries).*?)(?:#|\/|\?|\&|$)',
            'username': r'user\/(.*)(?:\?|$|\/)',
            'video': r'(?:(?:watch\?.*?v=(.*?)(?:#.*)?)|youtu\.be\/(.*?)(?:\?.*)?|embed\/(.*?)(?:\?.*))(?:#|\&|\/|$)'
        }

        Config = configparser.ConfigParser()
        mydir = os.path.dirname(os.path.abspath(__file__))
        Config.read(os.path.join(mydir, '..', "global_config.ini"))
        # Gets the key from the config file
        api_key = Config.get('GAPI', 'AUTH_KEY')
        # Splits the key(s) into a list - hacky workaround for using multi keys
        api_key_list = api_key.split(",")

        self.logger.debug('Running GAPI Datapull')
        super(GAPIProcess, self).__init__(GAPI_URLS, regexs, datapulls.GAPIpulls)
        self.api_key = api_key_list

    def _getData(self, url):
        self.logger.debug('Getting YouTube URL Redirect Data')
        try:
            url = requests.get(url).url #deals with redirects
        except requests.exceptions.ConnectionError:
            raise KeyError("Problem resolving url. No match.")
        try:
            force_order = ['video', 'channel', 'username', 'playlist']
            for i in force_order:
                match = re.search(self.regexs[i], url)
                if match:
                    self.logger.debug('Match Found')
                    break
            return (i, match.group(1))
        except AttributeError:
            self.logger.debug(u'No YouTube Match Found. URL: {}'.format(url))
            raise KeyError("No YouTube match found - %s" % url)


class DMAPIProcess(APIProcess):
    def __init__(self):
        # Initialize the logger
        self.logger = getSentinelLogger()

        DAILYMOTION_URLS = {
            'playlist': 'https://api.dailymotion.com/playlist/{}?fields=owner,owner.screenname,owner.url',
            'playlist videos': 'https://api.dailymotion.com/playlist/{}/videos?fields=owner,owner.screenname,owner.url',
            'username': 'https://api.dailymotion.com/user/{}?fields=id,screenname,url',
            'video': 'https://api.dailymotion.com/video/{}?fields=owner,owner.screenname,owner.url'
        }

        regexs = {
            'playlist': r'playlist\/(.+?)_',
            'video': r'(?:video\/|dai\.ly\/)(.+?)(?:#|\/|\?|$)',
        }

        self.logger.debug('Running DailyMotion Datapull')
        super(DMAPIProcess, self).__init__(DAILYMOTION_URLS, regexs, datapulls.DMpulls)

    def _getData(self, url):
        try:
            return super(DMAPIProcess, self)._getData(url)
        except KeyError:
            try:
                match = re.search(r'\.com\/(.+?)(?:\/|\?|$)', url)
                self.logger.debug('DM Match Found')
                return ('username', match.group(1))
            except AttributeError:
                self.logger.debug(u'No DM match found. URL: {}'.format(url))
                raise KeyError("No DM match found - %s" % url)


class VMOAPIProcess(APIProcess):
    def __init__(self):
        # Initialize the logger
        self.logger = getSentinelLogger()

        VIMEO_URLS = {
            'user': 'https://api.vimeo.com/users/{}?fields=uri,name,link',
            'playlist': 'https://api.vimeo.com/channels/{}?fields=user.uri,user.name,user.link',
            'playlist videos': 'https://api.vimeo.com/channels/{}/videos',
            'video': 'https://api.vimeo.com/videos/{}?fields=user.name,user.uri,user.link'
        }

        regexs = {
            'user': r'\/user(.+?)(?:\/|\?|$)',
            'playlist': r'\/channels\/(.+?)(?:\/|\?|$)'
        }

        self.logger.debug('Running Vimeo Datapull')
        super(VMOAPIProcess, self).__init__(VIMEO_URLS, regexs, datapulls.VMOpulls)

        Config = configparser.ConfigParser()
        mydir = os.path.dirname(os.path.abspath(__file__))
        Config.read(os.path.join(mydir, '..', "global_config.ini"))

        api_key = Config.get('VIMEO', 'AUTH_KEY')
        self.headers = {"Authorization": "Bearer " + api_key}

    def _getData(self, url):
        try:
            return super(VMOAPIProcess, self)._getData(url)
        except KeyError:
            try:
                match = re.search(r'\.com\/([^(?:user)].+?)(?:#|\/|\?|$)', url)
                self.logger.debug('Viemo Match Found')
                return ('video', match.group(1))
            except AttributeError:
                self.logger.debug('No Vimeo match found. URL: {}'.format(url))
                raise KeyError("No Vimeo match found - %s" % url)

    def _getJSONResponse(self, data, key):
        try:
            self.logger.debug('Getting Vimeo Data')
            return super(VMOAPIProcess, self)._getJSONResponse(data, key)
        except requests.exceptions.HTTPError:
            self.logger.debug('Getting Vimeo User Data')
            return super(VMOAPIProcess, self)._getJSONResponse(data, 'user')


class SCAPIProcess(APIProcess):
    def __init__(self):
        # Initialize the logger
        self.logger = getSentinelLogger()

        SOUNDCLOUD_URLS = {
            'all': 'https://api.soundcloud.com/resolve?url={}&client_id={}',
            'playlist videos': 'https://api.soundcloud.com/resolve?url={}&client_id={}',
        }

        regexs = {
            'all': r'(https:\/\/soundcloud\.com\/.*?)(?:#t?|$)',
        }

        self.logger.debug('Running SoundCloud Datapull')
        super(SCAPIProcess, self).__init__(SOUNDCLOUD_URLS, regexs, datapulls.SCpulls)

        Config = configparser.ConfigParser()
        mydir = os.path.dirname(os.path.abspath(__file__))
        Config.read(os.path.join(mydir, '..', "global_config.ini"))

        api_key = Config.get('SOUNDCLOUD', 'AUTH_KEY')
        self.api_key = api_key

    def getInformation(self, url):
        try:
            try:
                key, data = self._getData(url)
            except KeyError:
                return []
        
            key, jsonResponse = self._getJSONResponse(data, key)
            try:
                alldata = self.data_pulls[key](jsonResponse) or []
            except TypeError:
                return []
            self.logger.debug('Received SC data response')
            if jsonResponse['kind'] == 'playlist':
                self.logger.debug('Parsing SC playlist data')
                key, jsonResponse = self._getJSONResponse(data, 'playlist videos')
                try:
                    alldata += self.data_pulls['playlist videos'](jsonResponse)
                except TypeError:
                    pass
            return alldata
        except TypeError:
            self.logger.error(u'Error Getting Soundcloud Data. URL: {}'.format(url))
            return []


class FacebookAPIProcess(APIProcess):
    def __init__(self):

        self.logger = getSentinelLogger()

        Config = configparser.ConfigParser()
        mydir = os.path.dirname(os.path.abspath(__file__))
        Config.read(os.path.join(mydir, '..', "global_config.ini"))
        access_token = Config.get('FacebookAPI', 'ACCESS_TOKEN')

        self.api = facebook.GraphAPI(access_token=access_token)

        Facebook_URLS = {
            'video': lambda x: self.api.get_object(id=x, fields='from{id,link,name}'),
            'user': lambda x: self.api.get_object(id=x, fields='name,id,link')
        }

        regexs = {
            'video': r"k\.com\/.+?\/videos\/(.+?)(?:\/|\?|\&|$|#)",
            'user': r"k\.com\/(.+?)(?:\/|\?|\&|$|#)",
        }

        super(FacebookAPIProcess, self).__init__(Facebook_URLS, regexs, datapulls.FacebookPulls)

    def getInformation(self, url):
        self.logger.debug(u'Getting Information. URL: {}'.format(url))
        try:
            key, data = self._getData(url)
            self.logger.debug("Facebook key, data: {},{}".format(key,data))
        except KeyError:
            return []
        try:
            obj = self.API_URLS[key](data)
            self.logger.debug("Facebook object: {}".format(obj))
        except facebook.GraphAPIError:
            return []
        try:
            alldata = self.data_pulls[key](obj) or []
        except TypeError:
            return []

        return alldata

    def _getData(self, url):
        self.logger.debug('Getting URL Redirect Data')
        try:
            old_url = url
        except requests.exceptions.ConnectionError:
            raise KeyError("Problem resolving url. No match.")
        try:
            for i in ['video','user']:
                match = re.search(self.regexs[i], old_url)
                if match:
                    self.logger.debug('Match Found')
                    break
            return (i, match.group(1))
        except AttributeError:
            self.logger.debug(u'No Match Found. URL: {}'.format(url))
            raise KeyError("No match found - %s" % url)


class EtsyAPIProcess(APIProcess):
    def __init__(self):
        # Initialize the logger
        self.logger = getSentinelLogger()

        ETSY_URLS = {
            'user': 'https://openapi.etsy.com/v2/users/{}?includes=User&api_key={}',
            'listing': 'https://openapi.etsy.com/v2/listings/{}?includes=User&api_key={}',
            'shop': 'https://openapi.etsy.com/v2/shops/{}?includes=User&api_key={}',
        }

        regexs = {
            'listing': r'\/listing\/(.+?)(?:\/|\?|$)',
            'user': r'\/people\/(.+?)(?:\/|\?|$)',
            'shop': r'\/shop\/(.+?)(?:\/|\?|$)',
        }

        self.logger.debug('Running Etsy Datapull')
        super(EtsyAPIProcess, self).__init__(ETSY_URLS, regexs, datapulls.EtsyPulls)

        Config = configparser.ConfigParser()
        mydir = os.path.dirname(os.path.abspath(__file__))
        Config.read(os.path.join(mydir, '..', "global_config.ini"))

        api_key = Config.get('EtsyAPI', 'AUTH_KEY')
        self.api_key = api_key


class SentinelDatabase(Zion, TheTraveler):
    pass


class Memcache(object):
    def __init__(self, server_address=serverhost, server_port=11211):
        # Initialize the logger
        self.logger = getSentinelLogger()

        self.memclient = memcache.Client(['{0}:{1}'.format(server_address, server_port)], debug=0)
        self.memstats = MemcachedStats()
        self.memstats = MemcachedStats(server_address, server_port)

    def get_new(self, keyString='tsb'):
        try:
            # Get keys in the Memcache
            memcachekeys = self.memstats.keys()

            for key in memcachekeys:
                if key.startswith(keyString):
                    # Get the item from the memcache
                    thing = self.memclient.get(key)
                    # Delete it from memcache
                    self.memclient.delete(key)
                    yield thing

        except Exception:
            self.logger.error('Unable to access memcache queue')

    def add(self, thing, keyString='tsb'):
        self.memclient.add("{}_{}".format(keyString, thing.fullname), thing)
        #self.logger.debug(u'Added {} to memcache queue from {}'.format(thing.fullname, thing.subreddit))

    def add_polo(self, botname='thesentinelbot'):
        self.memclient.set("{}_{}".format('polo', botname), botname)
        self.logger.info(u'Responding to marco_polo.')
