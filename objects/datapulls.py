GAPIpulls = { 
    'video': (lambda x: [{
        'media_author': (i['snippet']['channelTitle']),
        'media_channel_id': str(i['snippet']['channelId']),
        'media_channel_url': 'https://www.youtube.com/channel/' + str(i['snippet']['channelId']),
        'media_platform': 'YouTube',
        'media_id': str(i['id']),
        } for i in x['items']]),
    'playlist': (lambda x: [{
        'media_author': (i['snippet']['channelTitle']),
        'media_channel_id': str(i['snippet']['channelId']),
        'media_channel_url': 'https://www.youtube.com/channel/' + str(i['snippet']['channelId']),
        'media_platform': 'YouTube',
        'media_id': str(i['id']),
        } for i in x['items']]),
    'playlist videos': (lambda x: [{
        'media_author': (i['snippet']['channelTitle']),
        'media_channel_id': str(i['snippet']['channelId']),
        'media_channel_url': 'https://www.youtube.com/channel/' + str(i['snippet']['channelId']),
        'media_platform': 'YouTube',
        'media_id': str(i['id']),
        } for i in x['items']]),
    'username': (lambda x: [{
        'media_author': (i['snippet']['title']),
        'media_channel_id': str(i['id']),
        'media_channel_url': 'https://www.youtube.com/channel/' + str(i['id']),
        'media_platform': 'YouTube',
        'media_id': str(i['id']),
        } for i in x['items']]),
    'channel': (lambda x: [{
        'media_author': (i['snippet']['title']),
        'media_channel_id': str(i['id']),
        'media_channel_url': 'https://www.youtube.com/channel/' + str(i['id']),
        'media_platform': 'YouTube',
        'media_id': str(i['id']),
        } for i in x['items']]),
}

TwitterAPIPulls = {
    'user': (lambda x: [{
            'media_author': x.screen_name,
            'media_channel_id': str(x.id),
            'media_channel_url': 'https://twitter.com/' + str(x.screen_name),
            'media_platform': "Twitter",
            'media_id': ''
        }])
}

VidmeAPIPulls = {
    'video': (lambda x: [{
        'media_author': (x['video']['user']['username']),
        'media_channel_id': (x['video']['user']['user_id']),
        'media_channel_url': (x['video']['user']['full_url']),
        'media_platform': 'Vidme',
        'media_id': (x['video']['url'])
        }])
}

TwitchAPIPulls = {
    'user': (lambda x: [{
        'media_author': (i['display_name']),
        'media_channel_id': (i['_id']),
        'media_channel_url': 'https://twitch.tv/' + i['name'],
        'media_platform': ('Twitch'),
        'media_id': '',
        } for i in x['users']])
}

DMpulls = {
    'video': (lambda x: [{
        'media_author': (x['owner.screenname']),
        'media_channel_id': str(x['owner']),
        'media_channel_url': x['owner.url'],
        'media_platform': 'DailyMotion',
        'media_id': ''
        }]),
    'playlist': (lambda x: [{
        'media_author': (x['owner.screenname']),
        'media_channel_id': str(x['owner']),
        'media_channel_url': x['owner.url'],
        'media_platform': 'DailyMotion',
        'media_id': ''
        }]),
    'playlist videos': (lambda x: [{
        'media_author': (i['owner.screenname']),
        'media_channel_id': str(i['owner']),
        'media_channel_url': x['owner.url'],
        'media_platform': 'DailyMotion',
        'media_id': '',
        } for i in x['list']
        ]),
    'username': (lambda x: [{
        'media_author': (x['screenname']),
        'media_channel_id': str(x['id']),
        'media_channel_url': x['owner.url'],
        'media_platform': 'DailyMotion',
        'media_id': ''
        }])
}

VMOpulls = {
    'user': (lambda x: [{
        'media_author': (x['name']),
        'media_channel_id': str(x['uri']),
        'media_channel_url': x['link'],
        'media_platform': 'Vimeo',
        'media_id': '',
        }]),
    'playlist': (lambda x: [{
        'media_author': (i['name']),
        'media_channel_id': str(i['uri']),
        'media_channel_url': x['link'],
        'media_platform': 'Vimeo',
        'media_id': '',
        } for i in x['user']]),
    'playlist videos': (lambda x: [{
        'media_author': (i['user']['uri']),
        'media_channel_id': str(i['user']['uri']),
        'media_channel_url': i['user']['link'],
        'media_platform': 'Vimeo',
        'media_id': '',
        } for i in x['data']]),
    'video': (lambda x: [{
        'media_author': (x['user']['name']),
        'media_channel_id': str(x['user']['uri']),
        'media_channel_url': x  ['user']['link'],
        'media_platform': 'Vimeo',
        'media_id': '',
        }])
}

SCpulls = {
    'all': lambda x: ([{
        'media_author': x['user']['username'],
        'media_channel_id': str(x['user']['id']),
        'media_channel_url': x['user']['uri'],
        'media_platform': 'SoundCloud',
        'media_id': ''
    } if x['kind'] != 'user' else {
        'media_author': (x['username']),
        'media_channel_id': str(x['id']),
        'media_channel_url': x['uri'],
        'media_platform': 'SoundCloud',
        'media_id': ''
    }]),
    'playlist videos': (lambda x: [{
        'media_author':   i['user']['username'],
        'media_channel_id': str(i['user']['id']),
        'media_channel_url': i['user']['uri'],
        'media_platform': 'SoundCloud',
        'media_id': ''
        } for i in x['tracks']])
    }

FacebookPulls = {
    'video': lambda x: ([{
            'media_author': x['from']['name'],
            'media_channel_id': x['from']['id'],
            'media_channel_url': x['from']['link'],
            'media_platform': 'Facebook',
            'media_id': '',
        }]),
    'user': lambda x: [{
        'media_author': x['name'],
        'media_channel_id': x['id'],
        'media_channel_url': x['link'],
        'media_platform': 'Facebook',
        'media_id': '',
    }]
}

EtsyPulls = {
    'user':lambda x: ([{
            'media_author': x['results'][0]['login_name'],
            'media_channel_id': str(x['results'][0]['user_id']),
            'media_channel_url': 'https://www.etsy.com/people/' + x['results'][0]['login_name'],
            'media_platform': 'Etsy',
            'media_id': '',
        }]),
    'shop':lambda x: ([{
            'media_author': x['results'][0]['User']['login_name'],
            'media_channel_id': str(x['results'][0]['User']['user_id']),
            'media_channel_url': 'https://www.etsy.com/people/' + x['results'][0]['User']['login_name'],
            'media_platform': 'Etsy',
            'media_id': '',
        }]),
    'listing': lambda x: ([{
            'media_author': x['results'][0]['User']['login_name'],
            'media_channel_id': str(x['results'][0]['User']['user_id']),
            'media_channel_url': 'https://www.etsy.com/people/' + x['results'][0]['User']['login_name'],
            'media_platform': 'Etsy',
            'media_id': '',
        }])
}

