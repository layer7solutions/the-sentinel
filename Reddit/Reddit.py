import praw, re, time, requests, sys, threading, sre_constants
from datetime import datetime
from collections import deque
from ..helpers.responses import *
from ..helpers import getSentinelLogger, SlackNotifier, ShadowbanDatabase
from ..objects import Memcache
from ..ModLogger import ModLogger, ModListSync
from ..ModmailArchiver import ModmailArchiver
from ..exceptions import TooFrequent
import prawcore.exceptions


class SentinelInstance:
    # Done this way so the inits can be done threaded.
    def __init__(self):
        pass

    def object_init(self, oauth, queue, masterClass):
        # full logger setup
        self.logger = getSentinelLogger()

        # create reddit instance here
        self.r = oauth.login()
        # self.r.config.log_requests = 0
        self.me = self.r.user.me()
        self.logger.debug("Started Instance: {}".format(self.r.user.me()))
        self.subsModdedTemp = self.r.user.moderator_subreddits(limit=None)

        self.subsModded = [i for i in self.subsModdedTemp]
        self.subsModdedWrite = [(i, i.subscribers) for i in self.subsModded]
        self.logger.info(
            "{} | Modding {} users in {}".format(
                self.me, self.subCount, [str(x) for x in self.subsModded]
            )
        )
        self.modMulti = self.r.subreddit("mod")
        self.globalBlacklistSubs = ["YT_Killer", "TheSentinelBot"]
        self.subextractor = re.compile("r\/(.*)")

        # this is fucking awful. it's just a list of moderators of the above two subs.
        self.globalBlacklistMods = list(
            set(
                [
                    red
                    for sub in self.globalBlacklistSubs
                    for red in self.r.subreddit(sub).moderator()
                ]
            )
        )
        self.removalQueue = queue
        self.masterClass = masterClass
        self.messageSub = "Layer7"
        self.cache = Memcache()
        self.subscriberLimit = 20_000_000
        self.notifier = SlackNotifier()
        self.modlogger = ModLogger(self.r, [str(i) for i in self.subsModded])
        self.modmailArchiver = ModmailArchiver(
            self.r, [str(i) for i in self.subsModded]
        )
        self.edited_done = deque()
        self.shadowban_db = ShadowbanDatabase()

        self.can_global_action = [
            self.r.redditor("thirdegree"),
            self.r.redditor("d0cr3d"),
            self.r.redditor("kwwxis"),
        ]

        self.blacklisted_subs = ["pokemongo"]
        self.minSubsLimit = 40

    def __str__(self):
        return self.me.name

    def clearQueue(self):
        processed = []
        while not self.removalQueue.empty():
            thing = self.removalQueue.get()
            things = self.r.info([thing.fullname])
            for thing in things:
                try:
                    message = self.masterClass.getInfo(thing)
                except KeyError:
                    message = [
                        {
                            "media_author": None,
                            "media_link": None,
                            "media_channel_id": None,
                            "media_platform": None,
                        }
                    ]

                if isinstance(thing, praw.models.Submission):
                    perma = thing.shortlink
                else:
                    perma = "https://reddit.com" + thing.permalink

                seen = []
                messaged = False
                for item in message:
                    item["author"] = str(thing.author)
                    item["subreddit"] = str(thing.subreddit)
                    item["permalink"] = perma
                    if item["media_author"] not in seen and item["media_author"]:
                        messaged = True
                        self.notifier.send_message(str(thing.subreddit), item)
                        self.notifier.send_message("yt_killer", item)
                        seen.append(item["media_author"])
                if not messaged and self.user_shadowbanned(thing):
                    item = {
                        "author": str(thing.author),
                        "subreddit": str(thing.subreddit),
                        "permalink": perma,
                        "media_author": "BotBan",
                        "media_link": "BotBan",
                        "media_channel_id": "BotBan",
                        "media_platform": "BotBan",
                    }
                    self.notifier.send_message(str(thing.subreddit), item)
                    self.notifier.send_message("yt_killer", item)

                thing.mod.remove()
                self.masterClass.markActioned(thing, type_of="tsb")
                processed.append(thing.fullname)
        if processed:
            self.logger.info("{} | Removed items: {}".format(self.me, processed))

    def get_permissions(self, mod_name, subreddit):  # both strings
        perms = list(self.r.subreddit(subreddit).moderator())
        for mod in perms:
            if str(mod) == mod_name:
                self.logger.debug(
                    "{} | Moderator {} permissions {}".format(
                        self.me, mod, mod.mod_permissions
                    )
                )
                return mod.mod_permissions
        return []

    def canAction(self, thing, subreddit=None):
        try:
            if not thing:
                if any(
                    [subreddit.lower() == str(x).lower() for x in self.subsModded]
                ):  # stupid workaround for the oauth shit
                    self.logger.debug(
                        "Subreddit {} matches subs bot mods".format(subreddit)
                    )
                    return True
                return False
            if any(
                [
                    str(thing.subreddit).lower() == str(x).lower()
                    for x in self.subsModded
                ]
            ):  # stupid workaround for the oauth shit
                self.logger.debug(
                    "Thing {} matches subs bot mods".format(thing.fullname)
                )
                return thing
            return False
        except prawcore.exceptions.Forbidden:
            self.logger.debug("PRAW Forbidden Error")
            return False
        except IndexError:
            return False

    def forceSubModListSync(self):
        modlist = ModListSync(self.r)
        threads = []
        threads.append(threading.Thread(target=modlist.log))
        self.logger.info(f"Starting modlist sync")

        for thread in threads:
            thread.start()

    def forceModlogHistory(self, body, author):
        matchstring = "(?:\/?r\/(\w+)|(all))"
        match = re.findall(matchstring, body, re.I)
        if not match:
            return
        if match[0][1] == "all":
            subs = [str(i).lower() for i in self.subsModded]
            subs_asked = subs
        else:
            subs_asked = [i[0].lower() for i in match]
            subs = list(
                set(subs_asked) & set([str(i).lower() for i in self.subsModded])
            )
        if not subs:
            self.logger.info(
                "{} | Found no matching subs to force modlog history from {}".format(
                    self.me, subs_asked
                )
            )
            return
        modlogger = ModLogger(self.r, subs)
        threads = []
        for (
            sub
        ) in (
            modlogger.subs_intersec
        ):  # I'm not sure why, but this works far better than a single modlogger for all the subs to force
            temp = ModLogger(self.r, [sub])
            threads.append(threading.Thread(target=temp.log, args=(None, author)))
        if modlogger.modLogMulti:
            self.logger.info(
                "{} | Forcing Modlog history for subs: {}".format(
                    self.me, [str(i) for i in modlogger.subs_intersec]
                )
            )
        for thread in threads:
            thread.start()

    def forceModMailHistory(self, body, author):
        matchstring = "(?:\/?r\/(\w+)|(all))"
        match = re.findall(matchstring, body, re.I)
        if not match:
            return
        if match[0][1] == "all":
            subs = [str(i).lower() for i in self.subsModded]
            subs_asked = subs
        else:
            subs_asked = [i[0].lower() for i in match]
            subs = list(
                set(subs_asked) & set([str(i).lower() for i in self.subsModded])
            )
        if not subs:
            self.logger.info(
                "{} | Found no matching subs to force modmail history from {}".format(
                    self.me, subs_asked
                )
            )
            return
        modmailArchiver = ModmailArchiver(self.r, subs)
        threads = []
        for sub in modmailArchiver.subs_intersec:
            temp = ModmailArchiver(self.r, [sub])
            threads.append(threading.Thread(target=temp.log, args=(None, author)))
        if modmailArchiver.modMailMulti:
            self.logger.info(
                "{} | Forcing Mod Mail history for subs: {}".format(
                    self.me, [str(i) for i in modmailArchiver.subs_intersec]
                )
            )
        for thread in threads:
            thread.start()

    def checkInbox(self):
        for message in self.r.inbox.unread(limit=None):
            self.logger.info(
                "{} | Processing Unread Inbox, MailID: {}".format(self.me, message.name)
            )

            if (
                message.body.startswith("**gadzooks!")
                or message.body.startswith("gadzooks!")
                or message.subject.startswith("invitation to moderate")
            ):
                message.mark_read()
                self.acceptModInvite(message)
                continue

            # Ignore any messages meant for the Replied Bot
            if (
                    message.subject == "TSBRepliedBot"
                    or message.subject == "BungieReplied"
            ):
                continue
            # Mark unread for everything else
            message.mark_read()
            if (
                "force modlist sync" in message.subject.lower()
                and message.author in self.can_global_action
            ):
                self.masterClass.forceSubModListSync()

            if (
                "force modlog history" in message.subject.lower()
                and message.author in self.can_global_action
            ):
                self.masterClass.forceModlogHistory(message.body, str(message.author))

            if (
                "force modmail history" in message.subject.lower()
                and message.author in self.can_global_action
            ):
                self.masterClass.forceModMailHistory(message.body, str(message.author))

            if "add to user shadowban" in message.subject.lower():
                try:
                    subreddits, user = self.add_user_shadowban(message)
                    message.reply(
                        "User {} shadowbaned for subs {}".format(user, subreddits)
                    )
                except TypeError:
                    message.reply("User shadowban failed")

            if "remove from user shadowban" in message.subject.lower():
                try:
                    subreddits, user = self.remove_user_shadowban(message)
                    message.reply(
                        "User {} shadowban removed for subs {}".format(user, subreddits)
                    )
                except TypeError:
                    message.reply("User shadowban failed")

            if "add to blacklist" in message.subject.lower():
                self.addBlacklist(message)
                continue

            if "remove from blacklist" in message.subject.lower():
                self.removeBlacklist(message)
                continue

            if "You have been removed as a moderator from " in message.body:
                self.logger.info(
                    "{} | Removed from subreddit /r/{}".format(
                        self.me, str(message.subreddit)
                    )
                )
                self.masterClass.remove_subreddit(str(message.subreddit))
                self.subsModded = [
                    i for i in self.r.user.moderator_subreddits(limit=None)
                ]
                self.masterClass.writeSubs()
                self.logger.info(
                    "{} | Now mods {} users.".format(self.me, self.subCount)
                )
                self.modlogger = ModLogger(self.r, [str(i) for i in self.subsModded])
                self.modmailArchiver = ModmailArchiver(
                    self.r, [str(i) for i in self.subsModded]
                )
                continue

            if message.subject.strip().lower().startswith("moderator message from"):
                self.logger.debug("Moderator Message, ignoring".format(self.me))
                continue

            # Username Mentions
            if message.subject.strip().lower().startswith("username mention"):
                self.logger.debug(
                    "{} | Wonder if this username mention is love-mail or hate-mail. Meh, ignorning".format(self.me)
                )
                continue

            if message.distinguished == "admin":
                try:
                    message.reply(
                        "Hey! I'm a bot. If you need to get in contact with my owners, you want u/D0cR3d or u/Thirdegree! They can also be reached at r/Layer7. Nice talking to you!"
                    )
                    self.logger.info(
                        "{} | Oh hey! We got Admin Mail! MailID: {}".format(self.me,
                                                                            message.fullname)
                    )
                except prawcore.exceptions.Forbidden:
                    self.logger.error(
                        "Unable to reply to AdminMail. They probably disabled replies. MailID: {}".format(
                            message.fullname
                        )
                    )

    def checkContent(self):
        toAdd = []
        self.logger.info("{} | Getting Content".format(self.me))
        self.logger.debug("{} | Getting Reddit New".format(self.me))
        for post in self.modMulti.new(limit=200):
            if self.masterClass.redis.exists(f"{post.fullname}") == 0:
                # self.masterClass.isProcessed(post):
                self.logger.debug(
                    "{} | Added Post to toAdd - {}".format(self.me, post.fullname)
                )
                toAdd.append(post)

        self.logger.debug("{} | Getting Reddit Comments".format(self.me))
        for comment in self.modMulti.comments(limit=300):
            if self.masterClass.redis.exists(f"{comment.fullname}") == 0:
                self.logger.debug(
                    "{} | Added comment to toAdd - {}".format(self.me, comment.fullname)
                )
                toAdd.append(comment)

        self.logger.debug("{} | Getting Reddit Edited".format(self.me))
        editlist = []
        for edit in self.modMulti.mod.edited(limit=100):
            # stupid why would that make sesnse after edited
            if self.masterClass.redis.exists(f"{edit.fullname}") == 0:
                self.logger.debug(
                    "{} | Added edit to toAdd - {}".format(self.me, edit.fullname)
                )
                editlist.append(edit)

        self.logger.debug("{} | Getting Reddit Spam".format(self.me))
        for spam in self.modMulti.mod.spam(limit=200):
            if self.masterClass.redis.exists(f"{spam.fullname}") == 0:
                self.logger.debug(
                    "{} | Added spam to toAdd - {}".format(self.me, spam.fullname)
                )
                toAdd.append(spam)

        shadowbanned = []
        if toAdd + editlist:
            self.logger.debug("Adding {} items to cache".format(len(toAdd + editlist)))

            for i in toAdd + editlist:
                self.logger.debug("Adding {} to cache".format(i.fullname))
                if self.user_shadowbanned(i):
                    self.logger.info(
                        "{} | User {} botbanned in {}".format(
                            self.me, str(i.author), str(i.subreddit)
                        )
                    )
                    self.removalQueue.put(i)
                    shadowbanned.append(i)
                elif isinstance(i, praw.models.Comment) and (
                    "earthclassmail.com" in i.body
                ):
                    self.removalQueue.put(i)
                    self.logger.info(
                        "{} | ThingID: {}. Site (earthclassmail.com) is global banned for spam".format(
                            self.me, i.fullname
                        )
                    )
                else:
                    self.cache.add(i)
        self.masterClass.markProcessed(toAdd)
        for thing in shadowbanned:
            self.masterClass.markActioned(thing, type_of="botban")

    def user_shadowbanned(self, thing):
        if not str(thing.subreddit) in self.shadowbans:
            return False

        for i in self.shadowbans[str(thing.subreddit)]:
            try:
                if i.lower() == str(thing.author).lower():
                    return True
            except sre_constants.error:
                self.logger.warning(
                    "{} | Malformed regex in botban for subreddit - {}: {}".format(
                        "BotBan " + str(self.me), str(thing.subreddit), i
                    )
                )
        return False

    def add_user_shadowban(self, thing):
        try:
            regex_subreddits = "r\/(\w*)"
            regex_username = "u\/(\w*)"
            subs = re.findall(regex_subreddits, thing.body)
            user = re.search(regex_username, thing.subject)
            self.logger.info(
                "{} | Processessing add user shadowban {} for subs {}".format(
                    self.me, user.group(1), subs
                )
            )
        except AttributeError:
            return False
        if (not user) or (not subs):
            return False

        ok_subs = []
        for sub in subs:
            try:
                if thing.author in self.r.subreddit(sub).moderator():
                    ok_subs.append(sub)
            except prawcore.exceptions.Forbidden:
                pass
        args = {
            "subreddits": ok_subs,
            "username": user.group(1),
            "bannedby": str(thing.author),
            "bannedon": datetime.utcfromtimestamp(thing.created_utc),
        }

        if args["subreddits"] and self.shadowban_db.add_shadowban(args):
            return (args["subreddits"], args["username"])
        return False

    def get_moderators(self, subreddit):
        return list(self.r.subreddit(subreddit).moderator())

    def remove_user_shadowban(self, thing):
        regex_subreddits = "r/(\w*)"
        regex_username = "u/(\w*)"
        subs = re.findall(regex_subreddits, thing.body)
        user = re.search(regex_username, thing.subject)
        if (not user) or (not subs):
            return False
        ok_subs = []
        for sub in subs:
            try:
                if thing.author in self.r.subreddit(sub).moderator():
                    ok_subs.append(sub)
            except prawcore.exceptions.Forbidden:
                pass
        args = {
            "subreddits": ok_subs,
            "username": user.group(1),
            "bannedby": str(thing.author),
            "bannedon": datetime.utcfromtimestamp(thing.created_utc),
        }
        if args["subreddits"] and self.shadowban_db.remove_shadowban(args):
            return (args["subreddits"], args["username"])
        return False

    def addBlacklist(self, thing):
        sub_string = re.search(self.subextractor, thing.subject)
        if not sub_string:
            thing.reply(
                "I'm sorry, your message appears to be missing a subreddit specification.\n\nPlease try using [our site](http://beta.layer7.solutions/sentinel/edit/) if you are still having issues. Thanks."
            )
        subreddit = self.r.subreddit(sub_string.group(1))

        try:
            mods = [i for i in subreddit.moderator()]

            if self.me not in mods:
                thing.reply(ForbiddenResponse.format(self.getCorrectBot(subreddit)))
            elif thing.author in mods:
                self.logger.info(
                    "{} | Add To Blacklist request from: {}".format(
                        self.me, thing.author
                    )
                )
                try:
                    bl = self.masterClass.addBlacklist(thing, subreddit)
                    if bl:
                        thing.reply("Channel(s) added to the blacklist: {}".format(bl))
                    else:
                        thing.reply("Channel add failed.")
                except requests.exceptions.HTTPError:
                    self.logger.info("{} | Add to blacklist failed - HTTPError")
                    thing.reply(
                        "Channel add failed. If the channel you requested was from Soundcloud, this is a known bug by Soundcloud."
                    )

        except praw.exceptions.APIException:
            self.logger.warning(
                "PRAW Forbidden Error - Incorrect Sentinel Instance Messaged"
            )
            thing.reply(ForbiddenResponse.format(self.getCorrectBot(subreddit)))

    def removeBlacklist(self, thing):
        sub_string = re.search(self.subextractor, thing.subject)
        if not sub_string:
            thing.reply(
                "I'm sorry, your message appears to be missing a subreddit specification.\n\nPlease try using [our site](http://beta.layer7.solutions/sentinel/edit/) if you are still having issues. Thanks."
            )
        subreddit = self.r.subreddit(sub_string.group(1))

        try:
            mods = [i for i in subreddit.moderator()]

            if self.me not in mods:
                thing.reply(ForbiddenResponse.format(self.getCorrectBot(subreddit)))
            elif thing.author in mods:
                self.logger.info(
                    "{} | Remove From Blacklist request from: {}".format(
                        self.me, thing.author
                    )
                )
                try:
                    bl = self.masterClass.removeBlacklist(thing, subreddit)
                    if bl:
                        thing.reply(
                            "Channel(s) removed from the blacklist: {}".format(bl)
                        )
                except requests.exceptions.HTTPError:
                    pass
        except praw.exceptions.APIException:
            self.logger.warning(
                "PRAW Forbidden Error - Incorrect Sentinel Instance Messaged"
            )
            thing.reply(ForbiddenResponse.format(self.getCorrectBot(subreddit)))

    @property
    def subCount(self):
        return sum([i.subscribers for i in self.subsModded])

    def acceptModInvite(self, message):
        try:
            if self.masterClass.getBot(message.subreddit):
                message.reply(
                    "You already have an active Sentinel account. We appreciate the enthusiasm, though!\n\nIf you recently removed an old Sentinel account, please wait ~5 minutes to add a new one to allow for processing time."
                )
                message.mark_read()
                self.logger.info("Bot already mods /r/{}".format(message.subreddit))
            elif message.subreddit.display_name.lower() in self.blacklisted_subs:
                message.reply("This subreddit is restriced from using this bot.")
                message.mark_read()
                self.logger.info(
                    "Subreddit blacklisted from /r/{}".format(message.subreddit)
                )
            elif message.subreddit.subscribers < self.minSubsLimit and not any(
                i in self.can_global_action for i in message.subreddit.moderator()
            ):
                message.reply(
                    "I'm sorry, you need to have {} or more subscribers before a Sentinel account can be added to your subreddit. If your need is critical, you can contact u/thirdegree or u/D0cR3d to have this restriction temporarily lifted.".format(
                        self.minSubsLimit
                    )
                )
                message.mark_read()
                self.logger.info("Subreddit Too small - r/{}".format(message.subreddit))
            elif self.subCount + message.subreddit.subscribers <= self.subscriberLimit:
                message.subreddit.mod.accept_invite()
                message.mark_read()
                self.subsModded.append(message.subreddit)
                self.logger.info(
                    "{} | Accepted mod invite for /r/{}".format(
                        self.me, message.subreddit
                    )
                )
                self.logger.info(
                    "{} | Now mods {} users".format(self.me, self.subCount)
                )

                self.masterClass.add_subreddit(
                    str(message.subreddit), str(self.me), message.subreddit.subscribers
                )
                self.masterClass.writeSubs()
                self.save_permissions(str(message.subreddit))
                self.forceModlogHistory("r/" + str(message.subreddit), None)
                self.modlogger = ModLogger(self.r, [str(i) for i in self.subsModded])
                self.modmailArchiver = ModmailArchiver(
                    self.r, [str(i) for i in self.subsModded]
                )

                self.masterClass.websync.ping_accept(str(message.subreddit), self)

                # Welcome PM
                message.reply(
                    "Thank you for the invite.\n\nI am a bot. You can read more about me at /r/TheSentinelBot and https://layer7.solutions. I'm designed to help fight media spam in your subreddit from both posts and comments. This includes YouTube, Twitter, and other media services. I have many more functions such as logging modlog actions and building nearly instant mod matrices, and more. I encourage you to explore our site and subreddit to learn how to best utilize my functionality and properly configure my settings.\n\nIf you have any questions, feel free to message r/TheSentinelBot or my owners at u/D0cR3d or u/Thirdegree."
                )
            else:
                try:
                    message.mark_read()
                    self.logger.info(
                        "{} | Bot at capacity. Attempting to find suitable replacement for /r/{}".format(
                            self.me, message.subreddit
                        )
                    )

                    botToAdd = self.masterClass.utility.get_available_bot(
                        self.subscriberLimit - message.subreddit.subscribers
                    )
                    if botToAdd:
                        message.subreddit.mod.accept_invite()
                        self.logger.debug(
                            "{} | Temp accepted mod invite for /r/{}. Attempting to invite u/{}".format(
                                self.me, message.subreddit, botToAdd
                            )
                        )

                        try:
                            message.subreddit.moderator.invite(
                                botToAdd, ["posts", "mail"]
                            )
                            self.logger.info(
                                "{} | Invited {} to mod /r/{}".format(
                                    self.me, botToAdd, message.subreddit
                                )
                            )
                            message.subreddit.moderator.leave()

                        except prawcore.exceptions.Forbidden:
                            self.logger.error(
                                "{} | Unable to invite bot /u/{} for /r/{}. Bot does not have correct perms".format(
                                    self.me, botToAdd, message.subreddit
                                )
                            )
                            message.subreddit.moderator.leave()
                            self.logger.debug(
                                "{} | Removed self from modding /r/{}".format(
                                    self.me, message.subreddit
                                )
                            )
                            message.reply(
                                "Sorry, but this bots account does not have the capacity available. Please invite /u/{} instead.".format(
                                    botToAdd
                                )
                            )

                except praw.exceptions.APIException:
                    self.logger.error(
                        "{} | Error inviting alt bot account u/{} to r/{}.".format(
                            self.me, botToAdd, message.subreddit
                        )
                    )

        except praw.exceptions.APIException:
            self.logger.error(
                "API Error Accepting Mod Invite for sub {}".format(message.subreddit)
            )
            # message.mark_read()
        except praw.exceptions.ClientException:
            self.logger.error(
                "Client Error Accepting Mod Invite for sub {}".format(message.subreddit)
            )
            # message.mark_read()
        except:
            self.logger.critical(
                "General Error Accepting Mod Invite: {}".format(message.name)
            )
            message.mark_read()

    def getCorrectBot(self, subreddit):
        return self.masterClass.getBot(subreddit)

    def save_permissions(self, subreddit=None):
        if subreddit:
            subs = [subreddit]
        else:
            subs = self.subsModded

        for sub in subs:
            for mod in self.r.subreddit(str(sub)).moderator():
                if mod == self.me:
                    perms = ","
                    self.masterClass.save_sentinel_permissions(
                        perms.join(mod.mod_permissions), str(sub)
                    )
                    break

    def track_self(self):
        self.masterClass.utility.track_agent(
            str(self), sum([i[1] for i in self.subsModdedWrite])
        )

    def start(self):
        try:
            self.track_self()
            self.save_permissions()
        except praw.exceptions.APIException:
            self.logger.error("Reddit HTTP Connection Error")
        while not self.masterClass.killThreads:
            # self.logger.debug('{} | Cycling..'.format(self.me.name))
            try:
                # Each agent shouldn't be updating this, only needs one
                # self.masterClass.done = set(self.masterClass.isProcessed(self.subsModded))
                self.shadowbans = self.shadowban_db.get_shadowbanned()
                # self.modMulti = self.r.subreddit('mod')

                self.masterClass.heartbeat(
                    "TheSentinelBot", str(self.me), self.subCount, len(self.subsModded)
                )
                self.checkContent()
                self.checkInbox()
                # self.checkModmail() # Not Used
                self.clearQueue()
                modlog_thread = threading.Thread(target=self.modlogger.log)
                modmailArchive_thread = threading.Thread(
                    target=self.modmailArchiver.log
                )
                modlog_thread.start()
                modmailArchive_thread.start()
                if self.masterClass.killThreads:
                    self.logger.info("{} | Acknowledging killThread".format(self.me))
            except praw.exceptions.APIException:
                self.logger.error("Reddit HTTP Connection Error")
            except Exception as e:
                self.logger.error(
                    "{} | General Error - {}: Sleeping 30".format(self.me, e)
                )
                time.sleep(30)
        else:
            modlog_thread.join()
            modmailArchive_thread.join()
